#pragma once

/**
 * ВНИМАНИЕ! Использование библиотеки Servo.h отключает ШИМ на пинах 9,10 (Uno)  
 * WARNING! Using the Servo.h library disables PWM on pins 9.10 (Uno)
 */

// Константы приложения
// Активность системы

// Constants app
// System activity
#define SYSTEM_ON true
// Открывать ли огонь по цели
// Whether to open fire on the target
#define FIRE true
// Передавать ли данные на компьютер, для графической части радара
// Whether to transfer data to a computer for the graphic part of the radar
#define REPORT true

// Настройки оружия
// Пин управления оружием
// Gun settings
// Pin gun control
#define GUN_PIN 11

// Настройки локатора
// Пин управления локатором
// Locator settings
// Pin locator control
#define LOCATOR_PIN 14
// Количество повторения измерения дистанции за один шаг серводвигателя
// The number of repetitions of the distance measurement in one step of the servomotor
#define COUNT_CALL 3

// Настройка прожектора
// Пин управления прожектором
// Setting the spotlight
// Pin spotlight control
#define SPOTLIGHT_PIN 3

// Настройки радара
// Radar settings
// Мин дистанция огня
// Min fire distance
#define MIN_DIST 20
// Макс дистанция огня
// max fire distance
#define MAX_DIST 30
// Пин управления сервоприводом орудия радара
// Pin control servo guns radar
#define GUN_SERVO_PIN 7
// Пин управления сервоприводом локатора радара
// Pin control servo radar locator
#define LOCATOR_SERVO_PIN 12
// расстояние между локатором и орудием
// чем точнее указано расстояние тем больше точность поворота орудия
// ВНИМАНИЕ! Измеряйте расстояние от орудия до приёмника дальномера
// distance between locator and instrument
// the more accurate the distance, the greater the accuracy of the tool rotation
// ATTENTION! Measure the distance from the implement to the range finder receiver
#define SERVO_DIST 10.5
// Минимальный угол поворота серводвигателя радара
// Minimum angle of rotation of the radar servo motor
#define MIN_SERVO_DEG 50
// Максимальный угол поворота серводвигателя радара
// Maximum angle of rotation of the radar servo motor
#define MAX_SERVO_DEG 130
// Number of degrees after an obstacle is detected before the start of the fire
#define COUNT_DEG_TARGET 5