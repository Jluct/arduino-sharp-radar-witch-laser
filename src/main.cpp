#include <Arduino.h>
#include "const.h"
#include <Servo.h>
#include "Locator.h"
#include "ArtilleryGun.h"
#include "Radar.h"
#include "Spotlight.h"
#include "ReportData.h"

// Для дебага
#define DEBUG false

#if REPORT
ReportData reporter;
#endif

Locator locator(LOCATOR_PIN, COUNT_CALL);
ArtilleryGun artilleryGun(GUN_PIN);
Spotlight spotlight(SPOTLIGHT_PIN);
RadarSettings settings = {LOCATOR_SERVO_PIN, GUN_SERVO_PIN, MIN_DIST, MAX_DIST, SERVO_DIST, MIN_SERVO_DEG, MAX_SERVO_DEG, COUNT_DEG_TARGET};
Radar radar(&locator, &artilleryGun, &spotlight, &settings);

void setup()
{
  Serial.begin(9600);
#if DEBUG
  Serial.println("---------SETUP--------");
  Serial.println("----------------------");
#endif

  radar.init();
  if (!radar.systemTest())
  {
    Serial.println("System ERROR!");
  }
}

void loop()
{
#if SYSTEM_ON
  radar.run();
#endif // DEBUG

#if REPORT
  reporter.serialReport(radar.getCurrentDegLocator(), radar.getDistance());
#endif // REPORT

#if FIRE
  if (radar.targetIsLocked())
  {
    radar.fire();
  }
#endif // FIRE
}
