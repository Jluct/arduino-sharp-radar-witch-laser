#pragma once

#include "ReportData.h"
#include <Arduino.h>

bool ReportData::serialReport(int angle, int distance)
{
    Serial.print(angle);    // Sends the current degree into the Serial Port
    Serial.print(",");      // Sends addition character right next to the previous value needed later in the Processing IDE for indexing
    Serial.print(distance); // Sends the distance value into the Serial Port
    Serial.print(".");      // Sends addition character right next to the previous value needed later in the Processing IDE for indexing

    return true;
}