#pragma once

#include "Locator.h"
#include "ArtilleryGun.h"
#include <Servo.h>
#include "Spotlight.h"
#include "RadarSettings.h"

class Radar
{
private:
  Locator *locator;
  ArtilleryGun *artilleryGun;
  Spotlight *spotlight;
  Servo locatorServo;
  Servo gunServo;
  RadarSettings *settings;
  unsigned int currentDegLocator;
  int countTargetLockedtDeg = 0;
  bool increase = true;
  bool targetLocked = false;
  bool ready = false;

public:
  Radar(Locator *locator, ArtilleryGun *artilleryGun, Spotlight *spotlight, RadarSettings *settings);
  void run();
  int calculateTarget(int deg);
  unsigned int getDistance();
  unsigned int getCurrentDegLocator();
  bool targetIsLocked();
  void fire();
  bool systemTest();
  void init();
};