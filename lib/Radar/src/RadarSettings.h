#pragma once

struct RadarSettings
{
    int locatorPinServo;
    int gunPinServo;
    unsigned int minDist;
    unsigned int maxDist;
    float servoDist;
    unsigned int minServoDeg;
    unsigned int maxServoDeg;
    int countDegTarget;
};
