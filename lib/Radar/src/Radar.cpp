#include <Radar.h>
#include "Spotlight.h"
#include <Arduino.h>

Radar::Radar(Locator *locator, ArtilleryGun *artilleryGun, Spotlight *spotlight, RadarSettings *settings)
{
    this->settings = settings;
    this->locator = locator;
    this->spotlight = spotlight;
    this->artilleryGun = artilleryGun;
}

void Radar::run()
{
    if (!this->ready)
    {
        return;
    }
#if DEBUG
    Serial.println("----------RUN---------");
    Serial.println(this->gunServo->read());
    Serial.println(this->locatorServo->read());
    Serial.println("----------------------");
#endif
    if (this->currentDegLocator > this->settings->minServoDeg && this->currentDegLocator <= this->settings->maxServoDeg)
    {
        this->locatorServo.write(this->currentDegLocator);
    }
    else
    {
        this->increase = !this->increase;
    }
    this->currentDegLocator = this->increase ? this->currentDegLocator + 1 : this->currentDegLocator - 1;
    unsigned int distance = this->locator->measureDistance();
    if (distance >= this->settings->minDist && distance <= this->settings->maxDist && this->countTargetLockedtDeg < this->settings->countDegTarget)
    {
        this->countTargetLockedtDeg++;
    }
    else if (this->countTargetLockedtDeg >= this->settings->countDegTarget)
    {
        this->targetLocked = true;
        this->countTargetLockedtDeg = 0;
    }
}

bool Radar::systemTest()
{
    if (this->gunServo.read() == 90 && this->locatorServo.read() == this->currentDegLocator)
    {
        this->ready = true;
    }

#if DEBUG
    Serial.println("----Servo position----");
    Serial.println(this->gunServo->read());
    Serial.println(this->locatorServo->read());
    Serial.println(this->currentDegLocator);
    Serial.println(this->ready);
    Serial.println("----------------------");
#endif

    return this->ready;
}

void Radar::init()
{
    this->locatorServo.attach(this->settings->locatorPinServo);
    this->gunServo.attach(this->settings->gunPinServo);
    this->gunServo.write(90);
    this->locatorServo.write(0);
    delay(1000);
    this->locatorServo.write(90);

    this->currentDegLocator = 90;
#if DEBUG
    Serial.println("---------INIT---------");
    Serial.println(this->locatorServo->read());
    Serial.println("----------------------");
#endif
    this->spotlight->run(255);
    delay(1000);
}

void Radar::fire()
{
    if (!this->targetLocked)
    {
        return;
    }
    int deg = this->calculateTarget(this->currentDegLocator);

    this->gunServo.write(deg);
    this->artilleryGun->fire();
    this->targetLocked = false;
}

/**
 * int deg 
 *      
 *      |\
 *     |  \
 *    |    \
 *   |      \
 *  |        \
 * |          \
 *|____________\ 
 * c2 = a2 + b2 - 2ab * cosγ
 * cosα =   (b2 + c2 − a2)/2bc;
 * β = 180° − α − γ
 * a = distServo
 * b = dist
 * gamma = deg;
 */
int Radar::calculateTarget(int deg)
{
#if DEBUG
    Serial.println("-----------DIST-----------");
    Serial.println(this->locator->getDistance());
    Serial.println("---------------------");
    Serial.println("---------servDist---------");
    Serial.println(this->settings->servoDist);
    Serial.println("---------------------");
    Serial.println("---------DEG---------");
    Serial.println(deg);
    Serial.println("---------------------");
    Serial.println("---------gammaDeg---------");
    Serial.println(gammaDeg);
    Serial.println("--------------------------");
#endif
    int rezult = 0;
    float x;
    float firstPartCalc = sq(this->locator->getDistance()) + sq(this->settings->servoDist);
    float secondPartCalc = 2 * this->locator->getDistance() * this->settings->servoDist * cos(deg);
    if (deg < 95)
    {
        x = sqrt(firstPartCalc - secondPartCalc);
    }
    else
    {
        x = sqrt(firstPartCalc + secondPartCalc);
    }
    float gunDeg = (this->locator->getDistance() * sin(deg)) / x;

    return (int)(180 - (asin(gunDeg) * 180 / PI));
}

unsigned int Radar::getDistance()
{
    return this->locator->getDistance();
}

unsigned int Radar::getCurrentDegLocator()
{
    return this->currentDegLocator;
}

bool Radar::targetIsLocked()
{
    return this->targetLocked;
}