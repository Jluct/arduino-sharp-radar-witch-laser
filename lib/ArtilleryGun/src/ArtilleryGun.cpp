#include "ArtilleryGun.h"
#include <Arduino.h>

ArtilleryGun::ArtilleryGun(int gunPin, int pwmPower, int fireDuration)
{
    this->gunPin = gunPin;
    this->pwmPower = pwmPower;
    this->fireDuration = fireDuration;
}

ArtilleryGun::~ArtilleryGun()
{
}

void ArtilleryGun::fire()
{
    analogWrite(this->gunPin, this->pwmPower);
    delay(this->fireDuration);
    analogWrite(this->gunPin, 0);
}