#pragma once

class ArtilleryGun
{
private:
  int gunPin;
  int pwmPower;
  int fireDuration;

public:
  ArtilleryGun(int gunPin, int pwmPower = 255, int fireDuration = 1000);
  ~ArtilleryGun();
  void fire();
};