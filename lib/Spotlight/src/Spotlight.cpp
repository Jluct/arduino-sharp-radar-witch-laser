#pragma once

#include "Spotlight.h"
#include <Arduino.h>

Spotlight::Spotlight(int pin)
{
    this->pin = pin;
}

void Spotlight::run(int power)
{
    analogWrite(this->pin, power);
}