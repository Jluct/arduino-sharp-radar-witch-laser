#pragma once

class Spotlight
{
  private:
    int pin;

  public:
    Spotlight(int pin);
    void run(int power);
};
