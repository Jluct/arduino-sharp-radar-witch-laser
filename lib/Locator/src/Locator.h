#pragma once

class Locator
{
private:
  int pin;
  float distance;
  unsigned int countCall = 10;

public:
  Locator(int pin, unsigned int countCall);
  ~Locator();
  unsigned int measureDistance();
  unsigned int getDistance();
  float callSensor();
};
