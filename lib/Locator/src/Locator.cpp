#include "Arduino.h"
#include "Locator.h"

Locator::Locator(int pin, unsigned int countCall)
{
    this->pin = pin;
    this->countCall = countCall;
}

Locator::~Locator()
{
}

unsigned int Locator::measureDistance()
{
    float results[3];
    for (int i = 0; i < this->countCall; i++)
    {
        results[i] = this->callSensor();
        delay(10);

#if DEBUG
        Serial.println("----------DISTANCE VERSION---------");
        Serial.println(results[i]);
        Serial.println("----------------------");
#endif
    }
    float result = 0;
    for (int i = 0; i < this->countCall; i++)
    {
        result += results[i];
    }
    this->distance = result / this->countCall;

#if DEBUG
    Serial.println("-------DISTANCE-------");
    Serial.println(this->distance);
    Serial.println("----------------------");
#endif

    return (unsigned int)this->distance;
}

float Locator::callSensor()
{
    float volts = analogRead(this->pin) * 0.00488758;
    float cm = 65 * pow(volts, -1.1904); // same in cm

    return cm;
}

unsigned int Locator::getDistance()
{
    return this->distance;
}